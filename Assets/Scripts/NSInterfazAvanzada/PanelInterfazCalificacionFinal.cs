﻿using NSEvaluacion;
using NsSeguridad;
using NSSistemaSolar;
using NSTraduccionIdiomas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace NSInterfazAvanzada 
{
	public class PanelInterfazCalificacionFinal : AbstractPanelInterfaz
	{
        #region members

        [SerializeField]
        private Text textValorCalificacion;

        [SerializeField]
        private Text textNombreUsuario;

        [SerializeField]
        private Evaluacion refEvaluacion;

        [SerializeField]
        private GameObject joyStick;

        [SerializeField]
        private PanelInterfazBotonesInterfazAfuera refPanelInterfazBotonesInterfazAfuera;

        [SerializeField]
        private SistemaSolarManager refSistemaSolarManager;

        [SerializeField]
        private PanelInterfazSeleccionFuncion refPanelInterfazSeleccionFuncion;

        [SerializeField]
        private ControlCamaraSistemaSolar refControlCamaraSistemaSolar;

        [SerializeField]
        private ControlCamaraPrincipal refControlCamaraPrincipal;

        [SerializeField]
        private clsHerramientasNotas refHerramientasNotas;

        [SerializeField]
        private clsEnvioPdf refEnvioPdf;

        [SerializeField]
        private ClsSeguridad refClsSeguridad;

        [SerializeField, Tooltip("Frases de aliento")]
        private TextMeshProUGUI textFelicitacion1;

        [SerializeField]
        private TextMeshProUGUI textFelicitacion2;

        [SerializeField]
        private string[] felicitacionMas60Porciento;

        [SerializeField]
        private string[] felicitacionMenos60Porciento;

        private DiccionarioIdiomas dic;
        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        private void OnEnable()
        {
            dic = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();

            SetTextValorCalificacion(refEvaluacion.GetCalificacionTotalRango());

            //Asignar textos calificacion
            if (refEvaluacion._refCalificacionSituacion.GetCalificacionTotal() > 0.6f)
            {
                textFelicitacion1.text= dic.Traducir( felicitacionMas60Porciento[0], felicitacionMas60Porciento[0]);
                textFelicitacion2.text = dic.Traducir(felicitacionMas60Porciento[1], felicitacionMas60Porciento[1]);
            }
            else
            {
                textFelicitacion1.text = dic.Traducir( felicitacionMenos60Porciento[0], felicitacionMenos60Porciento[0]);
                textFelicitacion2.text = dic.Traducir(felicitacionMenos60Porciento[1], felicitacionMenos60Porciento[1]);
            }            

            var tmpDatosSesion = refClsSeguridad.GetDatosSesion();
            SetTextNombreUsuario(tmpDatosSesion[0]);

        }

        #endregion

        #region private methods

        private void SetTextValorCalificacion(string argValorCalificacion)
        {
            textValorCalificacion.text = argValorCalificacion;
        }

        private void SetTextNombreUsuario(string argNombreUsuario)
        {
            textNombreUsuario.text = argNombreUsuario;
        }
        #endregion

        #region public methods

        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }        

        public void OnButtonAceptar()
        {
            ActivarPanel(false);
            joyStick.SetActive(true);
            refPanelInterfazBotonesInterfazAfuera.ActivarPanel();
            refSistemaSolarManager.SalirSituacion();
            refPanelInterfazSeleccionFuncion.SalirSituacion();
            refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoquePrimeraPersona);
            refControlCamaraPrincipal.SetOrthograpicCamera(false);
            refControlCamaraSistemaSolar.SetAccionActual(AccionesCamaraSistemaSolar.enfoquePrimeraPersona);
            refControlCamaraSistemaSolar.SetOrthograpicCamera(false);
            //refEnvioPdf.VisualizarPDF();
        }
        #endregion

        #region courutines

        #endregion
    }
}