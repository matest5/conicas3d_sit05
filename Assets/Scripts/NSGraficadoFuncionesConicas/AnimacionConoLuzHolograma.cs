﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public class AnimacionConoLuzHolograma : MonoBehaviour
	{

        #region members

        private float senoAnimacionRayos;
        [SerializeField]
        private Transform puntoFocoRotacionConosLuz;
        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
		
		}

 		// Use this for initialization
		void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{
            senoAnimacionRayos += Random.Range(-360, 360) * Time.deltaTime;

            transform.rotation = Quaternion.FromToRotation(Vector3.up, puntoFocoRotacionConosLuz.position - transform.position);
            //transform.position = puntoFocoRotacionConosLuz.position;
            //transform.localScale = Vector3.one * 0.5f + Vector3.one * Mathf.Sin(senoAnimacionRayos * Mathf.Deg2Rad) * 0.075f;
            transform.Rotate(new Vector3(0, Random.Range(-360, 360), 0), Space.Self);
		}

 		#endregion

 		#region private methods

 		#endregion

 		#region public methods

 		#endregion

 		#region courutines

 		#endregion 
	}
}