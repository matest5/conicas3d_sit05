﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public class FuncionCampana : AbstractDibujoFuncion
	{
        #region members

        private float h;

        private float k;

        private float r;
        #endregion

        #region accesors

        public float _h
        {
            set
            {
                h = value;
            }
        }

        public float _k
        {
            set
            {
                k = value;
            }
        }

        public float _r
        {
            set
            {
                r = value;
            }
        }
        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
		
		}

 		// Use this for initialization
		void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{

		}

        #endregion

        #region private methods

        #endregion

        #region public methods

        public override Vector3[] CalcularCordenadasFuncionMatematica()
        {
            var tmpListaPosicionesGrafica = new List<Vector3>();

            for (float tmpX = -1; tmpX <= 1; tmpX += 0.025f)
            {
                var tmpPointY = r * r - Mathf.Pow(tmpX + h, 2);

                if (tmpPointY < 0)
                {
                    tmpPointY *= -1;
                    tmpPointY = Mathf.Pow(tmpPointY, 1f / 3f) - k;
                    tmpPointY *= -1;
                }
                else
                {
                    tmpPointY = Mathf.Pow(tmpPointY, 1f / 3f) - k;
                }

                tmpListaPosicionesGrafica.Add(new Vector2(tmpX, tmpPointY));
            }

            posicionesRutaXY = tmpListaPosicionesGrafica.ToArray();
            return posicionesRutaXY;
        }

        public override void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true)
        {            
            CrearLineRenderer("LineaRectaPendiente", argMaterialDibujo, transform);
            SetPositionLineRenderer(posicionesRutaXY, argDibujarProgresivamente);
        }
        #endregion

        #region courutines

        #endregion
    }
}