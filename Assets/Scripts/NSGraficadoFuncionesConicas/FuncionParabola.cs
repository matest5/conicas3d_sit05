﻿using NSBoxMessage;
using NSGraficadoFuncionesConicas.FuncionAleatoria;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
    public class FuncionParabola : AbstractDibujoFuncion
    {
        #region members

        private float x;

        private float y;

        private float p;

        private float h;

        private float k;

        private Vector3 focoParabola;

        private DireccionParabola direccionParabola;

        [SerializeField]
        private GameObject prefabPlanetaFoco;

        [SerializeField]
        private GameObject prefabObjetoOrbita;

        [SerializeField]
        private GameObject prefabSol;

        private Transform transformObjetoOrbita;

        private float velocidadObjetoOrbita = 0.25f;

        #endregion

        #region accesors

        public float _p
        {
            set
            {
                p = value;
            }
            get
            {
                return p;
            }
        }

        public float _h
        {
            set
            {
                h = value;
            }
            get
            {
                return h;
            }
        }

        public float _k
        {
            set
            {
                k = value;
            }
            get
            {
                return k;
            }
        }

        public Vector3 _focoParabola
        {
            get
            {
                return focoParabola;
            }
        }

        public DireccionParabola _direccionParabola    
        {
            set
            {
                direccionParabola = value;
            }
            get
            {
                return direccionParabola;
            }
        }

        public float _velocidadObjetoOrbita
        {
            set
            {
                velocidadObjetoOrbita = value;
            }
            get
            {
                return velocidadObjetoOrbita;
            }
        }
        #endregion

        #region private methods

        public override Vector3[] CalcularCordenadasFuncionMatematica()
        {
            var tmpListaPosicionesGrafica = new List<Vector3>();

            switch (direccionParabola)
            {
                ///vertical
                case DireccionParabola.abajo:
                case DireccionParabola.arriba:

                    for (float tmpValueX = -1f; tmpValueX <= 1f; tmpValueX += 0.025f)
                    {
                        var tmpValueY = Mathf.Pow(tmpValueX - h, 2) / (4 * p) + k;
                        tmpValueY = Mathf.Clamp(tmpValueY, -1, 1);

                        if (tmpValueY == 1 || tmpValueY == -1)
                            continue;

                        tmpListaPosicionesGrafica.Add(new Vector3(tmpValueX, tmpValueY));
                    }

                    focoParabola = new Vector3(h, k + p);
                    break;

                ///horizontal
                case DireccionParabola.derecha:
                case DireccionParabola.izquierda:

                    for (float tmpValueY = -1f; tmpValueY <= 1f; tmpValueY += 0.025f)
                    {
                        var tmpValueX = Mathf.Pow(tmpValueY - k, 2) / (4 * p) + h;
                        tmpValueX = Mathf.Clamp(tmpValueX, -1, 1);

                        if (tmpValueX == 1 || tmpValueX == -1)
                            continue;

                        tmpListaPosicionesGrafica.Add(new Vector3(tmpValueX, tmpValueY));
                    }

                    focoParabola = new Vector3(h + p, k);
                    break;
            }

            posicionesRutaXY = tmpListaPosicionesGrafica.ToArray();
            return posicionesRutaXY;
        }

        public override void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true)
        {            
            CrearLineRenderer("Parabola", argMaterialDibujo, transform);
            SetPositionLineRenderer(posicionesRutaXY, argDibujarProgresivamente);
        }

        #endregion

        #region public methods

        #endregion

        #region courutines

        

        #endregion

    }
}