﻿using NSGraficadoFuncionesConicas.FuncionAleatoria;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public class PlanoCartesiano : MonoBehaviour
	{

        #region members

        private float anguloRotacionHorizontal;

        private float anguloRotacionVertical;

        private bool sePuedeRotar;

        [SerializeField, Header("Objetos dibujadores funciones"), Space(20)]
        private GameObject prefabDibujarParabola;

        [SerializeField]
        private GameObject prefabDibujarCircunferencia;

        [SerializeField]
        private GameObject prefabDibujarElipse;

        [SerializeField]
        private GameObject prefabDibujarFuncionCampana;

        [SerializeField]
        private GameObject prefabDibujarFuncionCubica; 

        [SerializeField]
        private GameObject prefabDibujarHiperbola;

        [SerializeField]
        private GameObject prefabDibujarLineaRectaSinPendiente;

        [SerializeField]
        private GameObject prefabDibujarRectaPendiente;

        [SerializeField, Header("Materiales Dibujo dibujadores funciones"), Space(20)]
        protected Material materialGraficaCorrecta;

        [SerializeField]
        protected Material materialGraficaIncorrecta;

        [SerializeField]
        protected Material materialGraficaSimulacion;
        #endregion

        #region accesors

        public Material _materialGraficaCorrecta
        {
            get
            {
                return materialGraficaCorrecta;
            }
        }

        public Material _materialGraficaIncorrecta
        {
            get
            {
                return materialGraficaIncorrecta;
            }
        }

        public Material _materialGraficaSimulacion
        {
            get
            {
                return materialGraficaSimulacion;
            }
        }

        #endregion

        #region monoBehaviour

        #endregion

        #region private methods

        private void RotarPlanoCartecianoVista3D()
        {

        }
        #endregion

        #region public methods

        public FuncionRectaSinPendiente CrearFuncionRectaSinPendiente(float argR, Transform argParent)
        {
            var tmpRefFuncionRectaSinPendiente = Instantiate(prefabDibujarLineaRectaSinPendiente, argParent).GetComponent<FuncionRectaSinPendiente>();
            tmpRefFuncionRectaSinPendiente._r = argR;
            tmpRefFuncionRectaSinPendiente.CalcularCordenadasFuncionMatematica();
            tmpRefFuncionRectaSinPendiente.transform.localPosition = Vector3.zero;
            return tmpRefFuncionRectaSinPendiente;
        }

        public FuncionCircunferencia CrearFuncionCircunferencia(float argH, float argK, float argR, Transform argParent)
        {
            var tmpRefFuncionCircunferencia = Instantiate(prefabDibujarCircunferencia, argParent).GetComponent<FuncionCircunferencia>();
            tmpRefFuncionCircunferencia._h = argH;
            tmpRefFuncionCircunferencia._k = argK;
            tmpRefFuncionCircunferencia._r = argR;
            tmpRefFuncionCircunferencia.CalcularCordenadasFuncionMatematica();
            tmpRefFuncionCircunferencia.transform.localPosition = Vector3.zero;
            return tmpRefFuncionCircunferencia;
        }

        public FuncionCubica CrearFuncionCubica(float argH, float argP, Transform argParent)
        {
            var tmpRefFuncionFuncionCubica = Instantiate(prefabDibujarFuncionCubica, argParent).GetComponent<FuncionCubica>();
            tmpRefFuncionFuncionCubica._h = argH;
            tmpRefFuncionFuncionCubica._p = argP;
            tmpRefFuncionFuncionCubica.CalcularCordenadasFuncionMatematica();
            tmpRefFuncionFuncionCubica.transform.localPosition = Vector3.zero;
            return tmpRefFuncionFuncionCubica;
        }

        public FuncionRectaPendiente CrearFuncionRectaPendiente(float argM, float argB, Transform argParent)
        {
            var tmpRefFuncionRectaPendiente = Instantiate(prefabDibujarRectaPendiente, argParent).GetComponent<FuncionRectaPendiente>();
            tmpRefFuncionRectaPendiente._m = argM;
            tmpRefFuncionRectaPendiente._b = argB;
            tmpRefFuncionRectaPendiente.CalcularCordenadasFuncionMatematica();
            tmpRefFuncionRectaPendiente.transform.localPosition = Vector3.zero;
            return tmpRefFuncionRectaPendiente;
        }

        public FuncionCampana CrearFuncionCampana(float argH, float argK, float argR, Transform argParent)
        {
            var tmpRefFuncionFuncionaCampana = Instantiate(prefabDibujarFuncionCampana, argParent).GetComponent<FuncionCampana>();
            tmpRefFuncionFuncionaCampana._h = argH;
            tmpRefFuncionFuncionaCampana._k = argK;
            tmpRefFuncionFuncionaCampana._r = argR;
            tmpRefFuncionFuncionaCampana.CalcularCordenadasFuncionMatematica();
            tmpRefFuncionFuncionaCampana.transform.localPosition = Vector3.zero;
            return tmpRefFuncionFuncionaCampana;
        }

        public FuncionParabola CrearFuncionParabola(float argH, float argK, float argP, Transform argParent, DireccionParabola argDireccionParabola)
        {
            var tmpRefFuncionParabola = Instantiate(prefabDibujarParabola, argParent).GetComponent<FuncionParabola>();
            tmpRefFuncionParabola._h = argH;
            tmpRefFuncionParabola._k = argK;
            tmpRefFuncionParabola._p = argP;
            tmpRefFuncionParabola._direccionParabola = argDireccionParabola;
            tmpRefFuncionParabola.CalcularCordenadasFuncionMatematica();
            tmpRefFuncionParabola.transform.localPosition = Vector3.zero;
            return tmpRefFuncionParabola;
        }

        public FuncionElipse CrearFuncionElipse(float argH, float argK, float argA, float argB, Transform argParent)
        {
            var tmpRefFuncionElipse = Instantiate(prefabDibujarElipse, argParent).GetComponent<FuncionElipse>();
            tmpRefFuncionElipse._h = argH;
            tmpRefFuncionElipse._k = argK;
            tmpRefFuncionElipse._a = argA;
            tmpRefFuncionElipse._b = argB;
            tmpRefFuncionElipse.CalcularCordenadasFuncionMatematica();
            tmpRefFuncionElipse.transform.localPosition = Vector3.zero;
            return tmpRefFuncionElipse;
        }

        public FuncionHiperbola CrearFuncionHiperbola(float argH, float argK, float argA, float argB, Transform argParent, SentidoHiperbola argSentidoHiperbola)
        {
            var tmpRefFuncionHiperbola = Instantiate(prefabDibujarHiperbola, transform).GetComponent<FuncionHiperbola>();
            tmpRefFuncionHiperbola._h = argH;
            tmpRefFuncionHiperbola._k = argK;
            tmpRefFuncionHiperbola._a = argA;
            tmpRefFuncionHiperbola._b = argB;
            tmpRefFuncionHiperbola._sentidoHiperbola = argSentidoHiperbola;
            tmpRefFuncionHiperbola.CalcularCordenadasFuncionMatematica();
            tmpRefFuncionHiperbola.transform.localPosition = Vector3.zero;
            return tmpRefFuncionHiperbola;
        }

        #endregion

        
    }

    public enum EcuacionSeleccioanda
    {
        rectaPendiente,
        parabolaArriba
    }
}