﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clsCordenada : MonoBehaviour
{

    public GameObject ejeX;
    public GameObject ejeY;
    public TextMesh cordenadas;


    private Vector3 posisionInicial;
    private Vector3 posAnteriorEjex;
    private Vector3 posAnteriorEJey;

    private clsHerramientaLaser X;
    private clsHerramientaLaser y;
    // Use this for initialization
    void Start()
    {
        X = ejeY.GetComponent<clsHerramientaLaser>();
        y = ejeX.GetComponent<clsHerramientaLaser>();

        posAnteriorEjex = ejeX.transform.position;
        posAnteriorEJey = ejeY.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        mtdMoverText();

    }

    private void mtdMoverText()
    {
        if (posAnteriorEjex.z != ejeX.transform.position.z)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, ejeX.transform.position.z);
            posAnteriorEjex = gameObject.transform.position;
            cordenadas.text = "[" + (X.cordenadaX * 100).ToString("0.00") + "," + (y.cordenadaY * 100).ToString("0.00") + "]"; 
        }

        if (posAnteriorEJey.x != ejeY.transform.position.x)
        {
            gameObject.transform.position = new Vector3(ejeY.transform.position.x, transform.position.y, transform.position.z);
            posAnteriorEJey = transform.position;
            cordenadas.text = "[" + (X.cordenadaX * 100).ToString("0.00") + "," + (y.cordenadaY * 100).ToString("0.00") + "]";  
        }

    }
    /// <summary>
    /// reinicia el valor de las cordenadas al punto 0,0 
    /// </summary>
    public void mtdResetValoresCordenadas()
    {
        cordenadas.text = "[0.00" + "," + "0.00]";

    }

}
