﻿using NSInterfazAvanzada;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSistemaSolar
{
	public class TriggerMesa : MonoBehaviour
	{
        #region members

        [SerializeField]
        private clsAdministradorSonido sonidos;
        [SerializeField]
        private SistemaSolarManager refSistemaSolarManager;

        [SerializeField]
        private PanelInterfazBotonesInterfazAfuera refPanelInterfazBotonesInterfazAfuera;
        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour
        private void Start()
        {
            //sonidos = GameObject.FindGameObjectWithTag("Sounds").GetComponent<clsAdministradorSonido>();
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.name.Equals("Player"))
            {
                refSistemaSolarManager.ColisionMesaHolograma();
                refPanelInterfazBotonesInterfazAfuera.ActivarPanel(false);
                sonidos.activarSOnidoConsola();
            }
        }

        #endregion

        #region private methods

        #endregion

        #region public methods

        #endregion

        #region courutines

        #endregion
    }
}