﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSistemaSolar
{
	public class ControlCamaraSistemaSolar : MonoBehaviour
	{

        #region members

        private Transform objetoFoco;

        [SerializeField]
        private float distanciaHastaObjetivo;

        private bool moverCamaraPosicionObjetivo;

        private float angulosEjeX;

        private float angulosEjeY;

        [SerializeField]
        private bool camaraPuedeRotar;

        private Vector3 previousMousePosition;

        [SerializeField]
        private AccionesCamaraSistemaSolar accionActual;

        private Camera refCamera;
        #endregion

        #region accesors

        public bool _moverCamaraPosicionObjetivo
        {
            set
            {
                moverCamaraPosicionObjetivo = value;
            }
        }
        #endregion

        #region monoBehaviour

        private void Start()
        {
            refCamera = GetComponent<Camera>();
        }

        // Update is called once per frame
        void Update ()
		{
            EjecutarAcciones();
        }

        #endregion

        #region private methods

        private void EjecutarAcciones()
        {
            switch (accionActual)
            {
                case AccionesCamaraSistemaSolar.enfoquePrimeraPersona:
                    EnfoquePrimerPersona();
                    break;

                case AccionesCamaraSistemaSolar.enfocarObjetivo:
                    EnfocarObjetivo();
                    break;
            }
        }

        private void MoverCamaraPosicionObjetivo()
        {
            if (moverCamaraPosicionObjetivo)
            {
                transform.position = Vector3.MoveTowards(transform.position, objetoFoco.localPosition, 0.1f);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(objetoFoco.localPosition - transform.position), 0.1f);
            }
            else
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, Vector3.zero, 0.1f);
                transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.identity, 0.1f);                
            }
        }

        private void EnfoquePrimerPersona()
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, 0.5f);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, 0.5f);
            refCamera.backgroundColor = Color.Lerp(refCamera.backgroundColor, new Color(0, 0, 0, 0f), 0.2f);
        }

        private void EnfocarObjetivo()
        {
            refCamera.backgroundColor = Color.Lerp(refCamera.backgroundColor, new Color(0, 0, 0, 0.5f), 0.2f);

            if (!camaraPuedeRotar)
                return;

            if (Input.GetMouseButtonDown(0))            
                previousMousePosition = Input.mousePosition;            
            else if (Input.GetMouseButton(0))
            {
                angulosEjeX += (previousMousePosition[0] - Input.mousePosition[0])*0.8f;
                angulosEjeY -= (previousMousePosition[1] - Input.mousePosition[1])*0.8f;
                angulosEjeY = Mathf.Clamp(angulosEjeY, -85, 20);
                previousMousePosition = Input.mousePosition;
            }

            var tmpPositionX = Mathf.Cos(angulosEjeX * Mathf.Deg2Rad) * Mathf.Cos(angulosEjeY * Mathf.Deg2Rad);
            var tmpPositionY = Mathf.Sin(angulosEjeY * Mathf.Deg2Rad);
            var tmpPositionZ = Mathf.Sin(angulosEjeX * Mathf.Deg2Rad) * Mathf.Cos(angulosEjeY * Mathf.Deg2Rad);

            var tmpVectorPosicion = new Vector3(tmpPositionX , tmpPositionY, tmpPositionZ);
            transform.position =  Vector3.Slerp(transform.position, objetoFoco.position + tmpVectorPosicion * distanciaHastaObjetivo, 0.05f);
            transform.rotation = Quaternion.LookRotation(objetoFoco.position - transform.position);
        }

 		#endregion

 		#region public methods

        public void SetObjetoFoco(Transform argObjetoFoco)
        {
            objetoFoco = argObjetoFoco;
            angulosEjeX = 90;
        }

        public void SetAccionActual(AccionesCamaraSistemaSolar argAccionesCamaraSistemaSolar)
        {
            accionActual = argAccionesCamaraSistemaSolar;
        }

        public void SetOrthograpicCamera(bool argIsOrthograpic)
        {
            GetComponent<Camera>().orthographic = argIsOrthograpic;
        }
        #endregion
    }

    public enum AccionesCamaraSistemaSolar
    {
        ningunaAccion,
        enfoquePrimeraPersona,
        enfocarObjetivo
    }
}